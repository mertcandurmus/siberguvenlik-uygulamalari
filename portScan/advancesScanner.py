
#!usr/bin/python

from socket import *
from optparse import *
from threading import *


def connScan(tgtHost, tgtPort):
	try:
		sock = socket(AF_INET,SOCK_STREAM)
		sock.connect()
		print 'open: %d '% tgtPort
	except:
		print 'close: %d' % tgtPort

	finally:
		sock.close()

def portScan(tgtHost,tgtPorts):
	try:
		tgIp = gethostbyname(tgtHost)
	except:
		print'unknown host %s' %tgtHost
        try:
                tgName = gethostbyaddr(tgtIp)
		print tgtName[0]
	except:
		print 'none'
	setdefaulttimeout(1)
	for tgtPort in tgtPorts:
		t = Thread(target=connScan,args(tgtHost,int(tgtPort)))
		t.start()






def main():
	parser=optparse.OptionParser('usage od program'+'-h<targt host> -p<target ports>')
	parser.add_option('-H',dest='tgtHost',type='string',help='specify')
	parser.add_option('-p',dest='tgtPorts',type='string',help='specify')
	(options, args)=parse._parse_args()
	tgtHost=options.tgtHost
	tgtPorts=str(options.tgtPort).split(',')
	portScan(tgtHost,tgtPorts)

if __main__ == '__main__':
	main()
